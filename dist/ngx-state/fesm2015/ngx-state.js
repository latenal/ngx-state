import { __decorate } from 'tslib';
import { ɵɵdefineInjectable, Injectable, NgModule } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

let NgxStateService = class NgxStateService {
    /**
     * Service class constructor
     */
    constructor() {
        this.states$ = new Map([]);
        /**
         * Do Nothing
         */
    }
    /**
     * Set an state property value
     */
    set(key, value) {
        if (this.states$.has(key)) {
            this.states$.get(key).next(value);
        }
        else {
            this.states$.set(key, new BehaviorSubject(value));
        }
        return this;
    }
    /**
     * Return the value of an specific state property
     */
    get(key) {
        return !this.states$.has(key) ? null : this.states$.get(key).getValue();
    }
    /**
     * Getter for the node list attribute
     */
    getObservable(key) {
        if (!this.states$.has(key)) {
            throw new Error(`App State property key "${key}" not found.`);
        }
        return this.states$.get(key).asObservable();
    }
};
NgxStateService.ɵprov = ɵɵdefineInjectable({ factory: function NgxStateService_Factory() { return new NgxStateService(); }, token: NgxStateService, providedIn: "root" });
NgxStateService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], NgxStateService);

let NgxStateModule = class NgxStateModule {
};
NgxStateModule = __decorate([
    NgModule({
        providers: [NgxStateService]
    })
], NgxStateModule);

/*
 * Public API Surface of ngx-state
 */

/**
 * Generated bundle index. Do not edit.
 */

export { NgxStateModule, NgxStateService };
//# sourceMappingURL=ngx-state.js.map
