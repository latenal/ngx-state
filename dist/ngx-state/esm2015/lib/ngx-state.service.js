import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
let NgxStateService = class NgxStateService {
    /**
     * Service class constructor
     */
    constructor() {
        this.states$ = new Map([]);
        /**
         * Do Nothing
         */
    }
    /**
     * Set an state property value
     */
    set(key, value) {
        if (this.states$.has(key)) {
            this.states$.get(key).next(value);
        }
        else {
            this.states$.set(key, new BehaviorSubject(value));
        }
        return this;
    }
    /**
     * Return the value of an specific state property
     */
    get(key) {
        return !this.states$.has(key) ? null : this.states$.get(key).getValue();
    }
    /**
     * Getter for the node list attribute
     */
    getObservable(key) {
        if (!this.states$.has(key)) {
            throw new Error(`App State property key "${key}" not found.`);
        }
        return this.states$.get(key).asObservable();
    }
};
NgxStateService.ɵprov = i0.ɵɵdefineInjectable({ factory: function NgxStateService_Factory() { return new NgxStateService(); }, token: NgxStateService, providedIn: "root" });
NgxStateService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], NgxStateService);
export { NgxStateService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXN0YXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtc3RhdGUvIiwic291cmNlcyI6WyJsaWIvbmd4LXN0YXRlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBYyxNQUFNLE1BQU0sQ0FBQzs7QUFLbkQsSUFBYSxlQUFlLEdBQTVCLE1BQWEsZUFBZTtJQUkxQjs7T0FFRztJQUNIO1FBTGlCLFlBQU8sR0FBc0MsSUFBSSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7UUFNeEU7O1dBRUc7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxHQUFHLENBQUMsR0FBVyxFQUFFLEtBQVU7UUFDaEMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkM7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQ25EO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxHQUFHLENBQUMsR0FBVztRQUNwQixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDMUUsQ0FBQztJQUVEOztPQUVHO0lBQ0ksYUFBYSxDQUFDLEdBQVc7UUFFOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzFCLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLEdBQUcsY0FBYyxDQUFDLENBQUM7U0FDL0Q7UUFFRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzlDLENBQUM7Q0FFRixDQUFBOztBQTVDWSxlQUFlO0lBSDNCLFVBQVUsQ0FBQztRQUNWLFVBQVUsRUFBRSxNQUFNO0tBQ25CLENBQUM7R0FDVyxlQUFlLENBNEMzQjtTQTVDWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE5neFN0YXRlU2VydmljZSB7XG5cbiAgcHJpdmF0ZSByZWFkb25seSBzdGF0ZXMkOiBNYXA8c3RyaW5nLCBCZWhhdmlvclN1YmplY3Q8YW55Pj4gPSBuZXcgTWFwKFtdKTtcblxuICAvKipcbiAgICogU2VydmljZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgKi9cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgLyoqXG4gICAgICogRG8gTm90aGluZ1xuICAgICAqL1xuICB9XG5cbiAgLyoqXG4gICAqIFNldCBhbiBzdGF0ZSBwcm9wZXJ0eSB2YWx1ZVxuICAgKi9cbiAgcHVibGljIHNldChrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IE5neFN0YXRlU2VydmljZSB7XG4gICAgaWYgKHRoaXMuc3RhdGVzJC5oYXMoa2V5KSkge1xuICAgICAgdGhpcy5zdGF0ZXMkLmdldChrZXkpLm5leHQodmFsdWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnN0YXRlcyQuc2V0KGtleSwgbmV3IEJlaGF2aW9yU3ViamVjdCh2YWx1ZSkpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm4gdGhlIHZhbHVlIG9mIGFuIHNwZWNpZmljIHN0YXRlIHByb3BlcnR5XG4gICAqL1xuICBwdWJsaWMgZ2V0KGtleTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gIXRoaXMuc3RhdGVzJC5oYXMoa2V5KSA/IG51bGwgOiB0aGlzLnN0YXRlcyQuZ2V0KGtleSkuZ2V0VmFsdWUoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXR0ZXIgZm9yIHRoZSBub2RlIGxpc3QgYXR0cmlidXRlXG4gICAqL1xuICBwdWJsaWMgZ2V0T2JzZXJ2YWJsZShrZXk6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG5cbiAgICBpZiAoIXRoaXMuc3RhdGVzJC5oYXMoa2V5KSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBBcHAgU3RhdGUgcHJvcGVydHkga2V5IFwiJHtrZXl9XCIgbm90IGZvdW5kLmApO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLnN0YXRlcyQuZ2V0KGtleSkuYXNPYnNlcnZhYmxlKCk7XG4gIH1cblxufVxuIl19