import { Observable } from 'rxjs';
export declare class NgxStateService {
    private readonly states$;
    /**
     * Service class constructor
     */
    constructor();
    /**
     * Set an state property value
     */
    set(key: string, value: any): NgxStateService;
    /**
     * Return the value of an specific state property
     */
    get(key: string): any;
    /**
     * Getter for the node list attribute
     */
    getObservable(key: string): Observable<any>;
}
