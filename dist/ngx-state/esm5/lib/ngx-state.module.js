import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { NgxStateService } from './ngx-state.service';
var NgxStateModule = /** @class */ (function () {
    function NgxStateModule() {
    }
    NgxStateModule = __decorate([
        NgModule({
            providers: [NgxStateService]
        })
    ], NgxStateModule);
    return NgxStateModule;
}());
export { NgxStateModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXN0YXRlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1zdGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtc3RhdGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUt0RDtJQUFBO0lBQThCLENBQUM7SUFBbEIsY0FBYztRQUgxQixRQUFRLENBQUM7WUFDUixTQUFTLEVBQUUsQ0FBQyxlQUFlLENBQUM7U0FDN0IsQ0FBQztPQUNXLGNBQWMsQ0FBSTtJQUFELHFCQUFDO0NBQUEsQUFBL0IsSUFBK0I7U0FBbEIsY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ3hTdGF0ZVNlcnZpY2UgfSBmcm9tICcuL25neC1zdGF0ZS5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgcHJvdmlkZXJzOiBbTmd4U3RhdGVTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBOZ3hTdGF0ZU1vZHVsZSB7IH1cbiJdfQ==