import { __decorate } from 'tslib';
import { ɵɵdefineInjectable, Injectable, NgModule } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

var NgxStateService = /** @class */ (function () {
    /**
     * Service class constructor
     */
    function NgxStateService() {
        this.states$ = new Map([]);
        /**
         * Do Nothing
         */
    }
    /**
     * Set an state property value
     */
    NgxStateService.prototype.set = function (key, value) {
        if (this.states$.has(key)) {
            this.states$.get(key).next(value);
        }
        else {
            this.states$.set(key, new BehaviorSubject(value));
        }
        return this;
    };
    /**
     * Return the value of an specific state property
     */
    NgxStateService.prototype.get = function (key) {
        return !this.states$.has(key) ? null : this.states$.get(key).getValue();
    };
    /**
     * Getter for the node list attribute
     */
    NgxStateService.prototype.getObservable = function (key) {
        if (!this.states$.has(key)) {
            throw new Error("App State property key \"" + key + "\" not found.");
        }
        return this.states$.get(key).asObservable();
    };
    NgxStateService.ɵprov = ɵɵdefineInjectable({ factory: function NgxStateService_Factory() { return new NgxStateService(); }, token: NgxStateService, providedIn: "root" });
    NgxStateService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], NgxStateService);
    return NgxStateService;
}());

var NgxStateModule = /** @class */ (function () {
    function NgxStateModule() {
    }
    NgxStateModule = __decorate([
        NgModule({
            providers: [NgxStateService]
        })
    ], NgxStateModule);
    return NgxStateModule;
}());

/*
 * Public API Surface of ngx-state
 */

/**
 * Generated bundle index. Do not edit.
 */

export { NgxStateModule, NgxStateService };
//# sourceMappingURL=ngx-state.js.map
