/*
 * Public API Surface of ngx-state
 */

export * from './lib/ngx-state.service';
export * from './lib/ngx-state.module';
