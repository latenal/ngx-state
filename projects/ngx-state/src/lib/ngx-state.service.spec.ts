import { TestBed } from '@angular/core/testing';

import { NgxStateService } from './ngx-state.service';

describe('NgxStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxStateService = TestBed.get(NgxStateService);
    expect(service).toBeTruthy();
  });
});
