import { NgModule } from '@angular/core';
import { NgxStateService } from './ngx-state.service';

@NgModule({
  providers: [NgxStateService]
})
export class NgxStateModule { }
