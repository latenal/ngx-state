import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NgxStateService {

  private readonly states$: Map<string, BehaviorSubject<any>> = new Map([]);

  /**
   * Service class constructor
   */
  constructor() {
    /**
     * Do Nothing
     */
  }

  /**
   * Set an state property value
   */
  public set(key: string, value: any): NgxStateService {
    if (this.states$.has(key)) {
      this.states$.get(key).next(value);
    } else {
      this.states$.set(key, new BehaviorSubject(value));
    }
    return this;
  }

  /**
   * Return the value of an specific state property
   */
  public get(key: string): any {
    return !this.states$.has(key) ? null : this.states$.get(key).getValue();
  }

  /**
   * Getter for the node list attribute
   */
  public getObservable(key: string): Observable<any> {

    if (!this.states$.has(key)) {
      throw new Error(`App State property key "${key}" not found.`);
    }

    return this.states$.get(key).asObservable();
  }

}
